let context = document.createElement('div');
let listContextItem = ['Copy', 'Cut', 'Paste', 'Delete'];
let section = document.getElementsByTagName('section')[0];

createContext(listContextItem);

document.getElementsByClassName('wrapper-page-painting')[0].addEventListener('contextmenu',()=>{
  event.preventDefault();
  context.style.top = event.pageY + 'px';
  context.style.left = event.pageX + 'px';
  section.appendChild(context);

  document.getElementById('copy-context').addEventListener('click', copyItem);
  document.getElementById('cut-context').addEventListener('click', cutItem);
  document.getElementById('paste-context').addEventListener('click', pasteItem);
  document.getElementById('delete-context').addEventListener('click', deleteItem);

});
document.addEventListener('click',()=>{
  event.preventDefault();
  if(document.getElementsByClassName('context')[0] != undefined)
    document.getElementsByClassName('context')[0].remove();
});

function copyItem() {console.log('copy')}
function cutItem() {console.log('cut')}
function pasteItem() {console.log('paste')}
function deleteItem() {console.log('delete')}

function createContext(listContextItem) {
  context.className = 'context';
  for(let i = 0 ; i < listContextItem.length ; i++ ){
    let contextItem = document.createElement('div');
    let innerItem = document.createElement('div');

    contextItem.className = 'context-item';
    innerItem.className = 'inner-item';
    contextItem.id = listContextItem[i].toLowerCase() + '-context';

    innerItem.innerHTML = listContextItem[i];
    contextItem.appendChild(innerItem);

    context.appendChild(contextItem);
  }

}


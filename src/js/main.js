let isShowSlideZoom = false;

let isDrag = false;
let isMouseDown;

let distanceUnits = 10;
let unitBefore = 10;
let unitCurrent = 0;
let lastX, lastY;

let offsetX = 0, offsetY = 0;

let zoom = 100;

let showRuler = false;
let dx = 0;
let dy = 0;
let w = 1;
let h = 1;

let zoomValue = document.getElementById('zoom-value');
let pagePainting = document.getElementById('panel');

let wrapperRulerHorizontal = document.getElementById('wrapper-ruler-horizontal');
let wrapperRulerVertical = document.getElementById('wrapper-ruler-vertical');
let limitWidth = Math.floor(window.innerWidth / 100);
let limitHeight = Math.floor(window.innerHeight / 100);

let unitStartWidth = 0, unitStartHeight = 0;
// drawRulerVertical1();
// function drawLine(x1,y1,x2,y2){
//   let rulerHorizontal = document.getElementById('ruler-horizontal');
//   let line = document.createElement('line');
//   line.setAttribute('x1',x1);
//   line.setAttribute('y1',y1);
//   line.setAttribute('x2',x2);
//   line.setAttribute('y2',y2);
//   line.setAttribute('stroke', 'black');

//   // rulerHorizontal.setAttribute('width','1920px');
//   // rulerHorizontal.setAttribute('height','40px');
//   rulerHorizontal.appendChild(line);
// }


// function drawText(x,y,textUnit) {
//   let unitHorizontal = document.getElementById('unit-horizontal');

//   let text = document.createElement('text');
//   text.x = x;
//   text.y = y;
//   text.innerHTML = textUnit;

//   unitHorizontal.appendChild(text);
// }
// function drawRulerVertical1() {
//   for(let i = 0 ; i < 1920 ; i+=10){
//     drawLine(i,0,i,15);
//   }
// }

drawRulerVertical(distanceUnits, 0, limitHeight, zoom);
drawRulerHorizontal(distanceUnits, 0, limitWidth, zoom);

function drawRulerHorizontal(distanceUnits, unitStart, limitRuler, zoom) {
  wrapperRulerHorizontal.style.display = showRuler ? '' : 'none';
  let unitOdd = 5;
  let pathHorizontal = '';
  let unitHorizontal = '';
  let unit = distanceUnits;
  for (let i = unitStart; i <= limitRuler; i++) {
    for (let j = 1; j <= 10; j++) {
      if (j == 10) {
        pathHorizontal = pathHorizontal + ` M${unit},0 ${unit},15 `;
        if (zoom < 50) {
          unitHorizontal = unitHorizontal + `<text x="${unit}"  y="26">${i % 2 == 0 ? i * 10 : ''}</text>`;
        }
        else {
          unitHorizontal = unitHorizontal + `<text x="${unit}"  y="26">${i * 10}</text>`;
        }
        unit += distanceUnits;
        break;
      }
      else if (j == 5) {
        if (Number(zoom) > 150) {
          unitHorizontal = unitHorizontal + `<text x="${unit}"  y="26">${i * 10 - 5}</text>`;
          unitOdd = i != 1 ? unitOdd + 10 : 5;
        }
        pathHorizontal = pathHorizontal + ` M${unit},0 ${unit},10 `;
      }
      else {
        if (zoom > 50) {
          pathHorizontal = pathHorizontal + ` M${unit},0 ${unit},5 `;
        }
      }
      unit += distanceUnits;
    }
  }
  document.getElementById('ruler-horizontal').innerHTML = `<path d="${pathHorizontal}" transform="translate(30 0)" style="stroke:#000000"/>`;
  document.getElementById('unit-horizontal').innerHTML = unitHorizontal;
}

function drawRulerVertical(distanceUnits, unitStart, limitRuler, zoom) {
  wrapperRulerVertical.style.display = showRuler ? '' : 'none';

  let pathVertical = '';
  let unitVertical = '';
  let unitOdd = 5;
  let unit = distanceUnits;
  for (let i = unitStart; i <= limitRuler; i++) {
    for (let j = 1; j <= 10; j++) {
      if (j == 10) {
        pathVertical = pathVertical + ` M0,${unit} 15,${unit} `;
        if (zoom < 50) {
          unitVertical = unitVertical + `<text x="25"  y="${unit}">${i % 2 == 0 ? i * 10 : ''}</text>`;
        }
        else {
          unitVertical = unitVertical + `<text x="25"  y="${unit}">${i * 10}</text>`;
        }
        unit += distanceUnits;
        break;
      }
      else if (j == 5) {
        if (Number(zoom) > 150) {
          unitVertical = unitVertical + `<text x="25"  y="${unit}">${i * 10 - 5}</text>`;
          unitOdd = i != 1 ? unitOdd + 10 : 5;
        }
        pathVertical = pathVertical + ` M0,${unit} 10,${unit} `;
      }
      else {
        if (zoom > 50) {
          pathVertical = pathVertical + ` M0,${unit} 5,${unit} `;
        }
      }
      unit += distanceUnits;
    }
  }
  document.getElementById('ruler-vertical').innerHTML = `<path d="${pathVertical}" transform="translate(0 30)" style="stroke:#000000"/>`;
  document.getElementById('unit-vertical').innerHTML = unitVertical;
}

function handleShowSlideZoom() {
  isShowSlideZoom = !isShowSlideZoom;
  if (isShowSlideZoom) {
    document.getElementsByClassName('slide-zoom')[0].classList.add('expand');
  }
  else {
    document.getElementsByClassName('slide-zoom')[0].classList.remove('expand');
  }
}

function handleZoom(percent) {
  zoom = Number(percent);
  zoomValue.innerHTML = percent + '%';
  // pagePainting.viewBox.baseVal.width = pagePainting.width.baseVal.value / (percent/100);
  // pagePainting.viewBox.baseVal.height = pagePainting.height.baseVal.value / (percent/100);
  // pagePainting.viewBox.baseVal.x = -((pagePainting.x.baseVal.value + pagePainting.width.baseVal.value/2) -  (percent/100) * (pagePainting.x.baseVal.value + pagePainting.width.baseVal.value/2) + offsetX) ;
  // pagePainting.viewBox.baseVal.y = -((pagePainting.y.baseVal.value + pagePainting.height.baseVal.value/2) -  (percent/100) * (pagePainting.y.baseVal.value + pagePainting.height.baseVal.value/2) + offsetY ) ;

  unitCurrent = percent / 10;
  if (unitBefore < unitCurrent) {
    distanceUnits = distanceUnits + (unitCurrent - unitBefore);
  }
  else {
    distanceUnits = distanceUnits - (unitBefore - unitCurrent);
  }

  limitWidth = Math.floor((window.innerWidth) / (distanceUnits * 10));
  limitHeight = Math.floor((window.innerHeight) / (distanceUnits * 10));


  handleShowUnit(offsetX, offsetY, distanceUnits);

  if (unitStartWidth <= 0 && 0 <= limitWidth) {
    wrapperRulerHorizontal.style.transform = `translate(${-10 * distanceUnits - dx * distanceUnits * 10 + offsetX}px)`;
    wrapperRulerVertical.style.transform = `translate(0, ${-10 * distanceUnits - dy * distanceUnits * 10 + offsetY}px)`;
    drawRulerHorizontal(distanceUnits, unitStartWidth, limitWidth, zoom);
    drawRulerVertical(distanceUnits, unitStartHeight, limitHeight, zoom);
  }
  else {
    if (zoom < 100) {
      let tX = -10 * distanceUnits + offsetX - dx * distanceUnits * 10;
      let tY = -10 * distanceUnits + offsetY - dy * distanceUnits * 10;
      if (tX == -(10 * distanceUnits)) {
        tX = 0;
        wrapperRulerHorizontal.style.transform = `translate(${tX}px)`;
      }
      if (tY == -(10 * distanceUnits)) {
        tY = 0;
        wrapperRulerHorizontal.style.transform = `translate(${tY}px)`;
      }
      drawRulerHorizontal(distanceUnits, unitStartWidth, limitWidth + unitStartWidth, zoom);
      drawRulerVertical(distanceUnits, unitStartHeight, limitHeight + unitStartHeight, zoom);
    }
    if (zoom > 100) {
      drawRulerHorizontal(distanceUnits, unitStartWidth, limitWidth + unitStartWidth, zoom);
      drawRulerVertical(distanceUnits, unitStartHeight, limitHeight + unitStartHeight, zoom);
    }
  }
  unitBefore = unitCurrent;
}


document.body.addEventListener('keydown', (e) => {
  if (e.which === 32) {
    isDrag = true;
  }
});

document.body.addEventListener('keyup', (e) => {
  isDrag = false;
});

pagePainting.addEventListener('mousedown', (e) => {
  if (isDrag) {
    lastX = e.pageX;
    lastY = e.pageY;
    isMouseDown = true;
    document.body.classList.add('no-select');
  }
})


pagePainting.addEventListener('mousemove', function (e) {
  if (isMouseDown && isDrag) {
    let translateDx = 0;
    // let translateDy = 0;
    offsetX = offsetX + e.pageX - lastX;
    offsetY = offsetY + e.pageY - lastY;
    pagePainting.viewBox.baseVal.x = -offsetX;
    pagePainting.viewBox.baseVal.y = -offsetY;
    lastX = e.pageX;
    lastY = e.pageY;

    if(offsetX == distanceUnits * 10 * (dx + 1 )){
      translateDx = 0;
    } else{
      translateDx = offsetX - distanceUnits * 10 * (dx + 1 );
      if(translateDx <= 0 && translateDx > (- distanceUnits * 10 * (dx + 1 ))){
        translateDx = offsetX - distanceUnits * 10 * (dx + 1 );
      }
      else{
        translateDx = (offsetX - distanceUnits * 10 * (dx + 1 )) % 100 - distanceUnits * 10;
      }
    }

    // if(offsetY == distanceUnits * 10 * (dy + 1 )){
    //   translateDy = 0;
    // } else{
    //   translateDy = offsetY - distanceUnits * 10 * (dy + 1 );
    //   if(translateDy <= 0 && translateDy > (- distanceUnits * 10 * (dy + 1 ))){
    //     translateDy = offsetY - distanceUnits * 10 * (dy + 1 );
    //   }
    //   else{
    //     translateDy = (offsetY - distanceUnits * 10 * (dy + 1 ))%100;
    //   }
    // }

    wrapperRulerHorizontal.style.transform = `translate(${translateDx}px)`;
    // wrapperRulerVertical.style.transform = `translate(0,${translateDy}px)`;  - distanceUnits * 10 * (dx + 1 )  - distanceUnits * 10 * (dy + 1 )
    // wrapperRulerHorizontal.style.transform = `translate(${offsetX == distanceUnits * 10 * (dx + 1) ? - distanceUnits * 10 : (offsetX - distanceUnits * 10 * (dx + 1)) % 100}px)`;
    wrapperRulerVertical.style.transform = `translate(0,${offsetY == distanceUnits * 10 * (dy + 1) ? - distanceUnits * 10 : (offsetY - distanceUnits * 10 * (dy + 1)) % 100}px)`;

    handleShowUnit(offsetX, offsetY, distanceUnits);
  }
});

function handleShowUnit(offsetX, offsetY, distanceUnits) {
  if (offsetX > distanceUnits * 10 * (dx + 1)) {
    unitStartWidth--;
    drawRulerHorizontal(distanceUnits, unitStartWidth, limitWidth + unitStartWidth, zoom);
    dx++;
    w = offsetX;
  }
  if (offsetX < w) {
    unitStartWidth++;
    drawRulerHorizontal(distanceUnits, unitStartWidth, limitWidth + unitStartWidth, zoom);
    w = w - distanceUnits * 10;
    dx--;
  }
  if (offsetY > distanceUnits * 10 * (dy + 1)) {
    unitStartHeight--;
    drawRulerVertical(distanceUnits, unitStartHeight, limitHeight + unitStartHeight, zoom);
    dy++;
    h = offsetY;
  }
  if (offsetY < h) {
    unitStartHeight++;
    drawRulerVertical(distanceUnits, unitStartHeight, limitHeight + unitStartHeight, zoom);
    h = h - distanceUnits * 10;
    dy--;
  }
}
pagePainting.addEventListener('mouseup', function () {
  isDrag = false;
  isMouseDown = false;
  document.body.classList.remove('no-select');
});

pagePainting.addEventListener('mouseleave', function () {
  isDrag = false;
  isMouseDown = false;
  document.body.classList.remove('no-select');
});

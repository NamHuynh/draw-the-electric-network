let isShowPanel = false;
let info = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit';
let childrenNumber;
let listLayout = [
  {
    imgLayout:'',
    info: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
  },
];
let panel = document.getElementById('panel');

let inputLayoutX = document.getElementById('layout-x');
let inputLayoutY = document.getElementById('layout-y');
let inputLayoutWidth = document.getElementById('layout-width');
let inputLayoutHeight = document.getElementById('layout-height');

createListItem(listLayout);

selectNodeSVG();

inputLayoutWidth.addEventListener('keypress', validateInput);
inputLayoutHeight.addEventListener('keypress', validateInput);

inputLayoutX.addEventListener('input',() => {
  if(childrenNumber != undefined)
    panel.children[childrenNumber].setAttribute('x',event.target.value == '' ? 0 : event.target.value);
});

inputLayoutY.addEventListener('input',() => {
  if(childrenNumber != undefined)
    panel.children[childrenNumber].setAttribute('y',event.target.value == '' ? 0 : event.target.value);
});

inputLayoutWidth.addEventListener('input',() => {
  if(childrenNumber != undefined)
    panel.children[childrenNumber].setAttribute('width',event.target.value == '' ? 0 : event.target.value);
});

inputLayoutHeight.addEventListener('input',() => {
  if(childrenNumber != undefined)
    panel.children[childrenNumber].setAttribute('height',event.target.value == '' ? 0 : event.target.value);
});

document.getElementsByClassName('btn-show-panel')[0].addEventListener('click', () => {
  isShowPanel = !isShowPanel;
  if (isShowPanel) {
    document.getElementsByClassName('info-item')[0].innerHTML = info;
    document.getElementsByClassName('panel__container')[0].classList.add('expand');
  } else {
    document.getElementsByClassName('panel__container')[0].classList.remove('expand');
  }
});

function selectNodeSVG() {
  for(let i = 0 ; i < panel.children.length ; i++){
    panel.children[i].addEventListener('click', ()=>{
      childrenNumber = i;
      inputLayoutX.value = panel.children[i].x.baseVal.value;
      inputLayoutY.value = panel.children[i].y.baseVal.value;
      inputLayoutWidth.value = panel.children[i].width.baseVal.value;
      inputLayoutHeight.value = panel.children[i].height.baseVal.value;
    });
  }
}

function validateInput(evt) {
  evt = (evt) ? evt : window.event;
  let charCode = (evt.which) ? evt.which : evt.keyCode;
  if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode !== 46) {
    evt.preventDefault();
  } else {
    return true;
  }
}

function createListItem(listLayout) {
  let listItems = document.getElementsByClassName('list-item')[0];
  for(let i = 0 ; i < listLayout.length ; i++){
    let wrapperItem = document.createElement('div');
    let imageItem = document.createElement('div');
    let nameItem = document.createElement('div');

    wrapperItem.className = 'wrapper-item';
    imageItem.className = 'image-item';
    nameItem.className = 'name-item';

    nameItem.innerHTML = listLayout[i].info;

    wrapperItem.appendChild(imageItem);
    wrapperItem.appendChild(nameItem);
    listItems.appendChild(wrapperItem);
  }

}


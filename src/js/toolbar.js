let x = 0 ;
let y = 0 ;
let width = 0 ;
let height = 0 ;
let rows = 0 ;
let cols = 0 ;

clickActive();

document.getElementById('mouse-tool').addEventListener('click',()=>{});
document.getElementById('move-tool').addEventListener('click',()=>{});
document.getElementById('square').addEventListener('click',()=>{});
document.getElementById('line').addEventListener('click',()=>{});
document.getElementById('grid-square').addEventListener('click',()=>{});
document.getElementById('hand-tool').addEventListener('click',()=>{});
document.getElementById('polyline-tool').addEventListener('click',()=>{});


document.getElementById('square').addEventListener('contextmenu',()=>{
  event.preventDefault();
  removeActive();
  document.getElementById('square').classList.add('active');
  if(document.getElementsByClassName('window-create-shape')[0] != undefined) {
    let windowCreateShape = document.getElementsByClassName('window-create-shape')[0];
    windowCreateShape.remove();
  }
  createListInput(['X','Y','Width','Height'],'75px');
});

document.getElementById('grid-square').addEventListener('contextmenu',()=>{
  event.preventDefault();
  removeActive();
  document.getElementById('grid-square').classList.add('active');
  if(document.getElementsByClassName('window-create-shape')[0] != undefined) {
    let windowCreateShape = document.getElementsByClassName('window-create-shape')[0];
    windowCreateShape.remove();
  }
  createListInput(['X','Y','Width','Height','Rows','Columns'],'155px');
});

function removeActive(){
  let toolbar = document.getElementsByClassName('toolbar')[0];
  for(let i = 0 ; i < toolbar.children.length ; i++){
    toolbar.children[i].classList.remove('active');
  }
}

function clickActive() {
  let toolbar = document.getElementsByClassName('toolbar')[0];
  for(let i = 0 ; i < toolbar.children.length ; i++){
    toolbar.children[i].addEventListener('click', ()=>{
      removeActive();
      toolbar.children[i].classList.add('active');
    });
  }
}

function createShape() {
  x = document.getElementById('inputX').value;
  y = document.getElementById('inputY').value;
  width = document.getElementById('inputWidth').value;
  height = document.getElementById('inputHeight').value;
  if(document.getElementById('inputRows') != undefined && document.getElementById('inputColumns') != undefined){
    rows = document.getElementById('inputRows').value;
    cols = document.getElementById('inputColumns').value;
  }
  document.getElementsByClassName('window-create-shape')[0].remove();
}

function cancelCreateShape() {
  document.getElementsByClassName('window-create-shape')[0].remove();
}

function createListInput(listInput,position) {
  CreateWindowCreateShape();
  let windowCreateShape = document.getElementsByClassName('window-create-shape')[0];
  let wrapperInput = document.getElementsByClassName('wrapper-input')[0];
  windowCreateShape.style.top = position;
  windowCreateShape.style.visibility = 'inherit';
  wrapperInput.innerHTML = '';
  for(let i = 0 ; i < listInput.length ; i++){
    let div = document.createElement('div');
    let label = document.createElement('label');
    let input = document.createElement('input');
    input.type = 'number';
    input.placeholder = '0';
    input.id = 'input' + listInput[i];
    if( i > 1){
      input.min = 0;
      input.addEventListener('keypress', (evt)=> {
        evt = (evt) ? evt : window.event;
        let charCode = (evt.which) ? evt.which : evt.keyCode;
        if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode !== 46) {
          evt.preventDefault();
        } else {
          return true;
        }
      });
    }
    label.innerHTML = listInput[i];
    div.appendChild(label);
    div.appendChild(input);
    wrapperInput.appendChild(div);
  }
}

function CreateWindowCreateShape(){
  let section = document.getElementsByTagName('section')[0];

  let windowCreateShape = document.createElement('div');
  let arrowLeft = document.createElement('div');
  let wrapperInput = document.createElement('div');
  let wrapperButton = document.createElement('div');
  let btnOK = document.createElement('div');
  let btnCancel = document.createElement('div');

  arrowLeft.className = 'arrow-left';
  wrapperInput.className = 'wrapper-input';
  wrapperButton.className = 'wrapper-button';
  windowCreateShape.className = 'window-create-shape';
  btnOK.className = 'btn-action';
  btnCancel.className = 'btn-action';

  btnOK.innerHTML = 'OK';
  btnCancel.innerHTML = 'Cancel';

  btnOK.addEventListener('click', createShape);
  btnCancel.addEventListener('click', cancelCreateShape);

  windowCreateShape.appendChild(arrowLeft);
  windowCreateShape.appendChild(wrapperInput);
  windowCreateShape.appendChild(wrapperButton);
  wrapperButton.appendChild(btnOK);
  wrapperButton.appendChild(btnCancel);

  section.appendChild(windowCreateShape);
}
